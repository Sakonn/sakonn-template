// The following code is based off a toggle menu by @Bradcomp
// source: https://gist.github.com/Bradcomp/a9ef2ef322a8e8017443b626208999c1

//var mixitup = require('mixitup');
// var mixer = mixitup('.mixContainer'); Mixer initialization moved to mixer file

(function() {
  var burger = document.querySelector('.burger');
  var menu = document.querySelector('#'+burger.dataset.target);
  burger.addEventListener('click', function() {
      burger.classList.toggle('is-active');
      menu.classList.toggle('is-active');
  });
})();

document.addEventListener('scroll', function(e){
  if (window.scrollY > 200) {
    document.querySelector('.back-to-top').classList.remove('is-hidden');
    document.querySelector('.back-to-top').classList.remove('animate__fadeOutDown');
    if (!document.querySelector('.back-to-top').classList.contains('animate__fadeInUp')) {
      document.querySelector('.back-to-top').classList.add('animate__fadeInUp');
    }
  } else {
    document.querySelector('.back-to-top').classList.remove('animate__fadeInUp');
    if (!document.querySelector('.back-to-top').classList.contains('is-hidden')) {
      document.querySelector('.back-to-top').classList.add('animate__fadeOutDown');
    }
    setTimeout(() => {
      if (window.scrollY < 200) {
        document.querySelector('.back-to-top').classList.add('is-hidden');
      }
    }, 2000);
  }
});

function scrollToTop() {
  window.scrollTo({
    top: 0, 
    left: 0,
    behavior: 'smooth'
  });
  
}